\contentsline {chapter}{Inhaltsverzeichnis}{i}{Doc-Start}%
\contentsline {chapter}{\numberline {1}Kryptowährungen und deren Technik}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Einleitung}{2}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Geschichte \& Entstehung}{2}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Vergleich zu alternativen Technologien}{4}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Genereller Aufbau Kryptowährungen}{9}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Distributed Ledger Technology}{9}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Blockchain}{15}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Softwareentwicklung in der Blockchain}{32}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Digitale Vermögenswerte}{40}{subsection.1.2.4}%
\contentsline {section}{\numberline {1.3}Anwendungsbereiche von Blockchain}{44}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Kryptowährungen im Vergleich}{44}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Dezentralisierte Apps}{52}{subsection.1.3.2}%
\contentsline {section}{\numberline {1.4}Fazit fürs Projekt ''Tradeplay''}{56}{section.1.4}%
\contentsline {chapter}{Anhang}{57}{section.1.4}%
\contentsline {section}{Abbildungsverzeichnis}{57}{section*.21}%
\contentsline {section}{Tabellenverzeichnis}{59}{section*.23}%
\contentsline {section}{Verzeichnis der Listings}{61}{section*.25}%
\contentsline {section}{Index}{63}{section*.27}%
\contentsline {section}{Literaturverzeichnis}{63}{section*.28}%
