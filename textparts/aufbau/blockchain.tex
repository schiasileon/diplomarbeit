\subsection{Blockchain}

''Blockchain'' ist als Gesprächsthema in der modernen Informatik kaum noch wegzudenken. Obwohl Blockchain ''nur'' eine spezielle Umsetzung dezentraler Ledger darstellt, hat sich diese in der Öffentlichkeit am meisten durchgesetzt, vor allem durch den Erfolg und der Nachrichten um das Bitcoin Protokoll. Aufgrund der großen Trendentwicklung rund um Blockchain wird in dem folgenden Kapitel vertiefend die Funktionsweise dieser erklärt.

\underline{\textbf{Begriffserklärung und Definitionen}}

Wenn man mit der Englischen Sprache umgehen kann, ist zu erkennen, dass sich der Begriff ganz einfach in zwei Teile zerlegen lässt. Alternativ kann man das Wort direkt ins deutsche übersetzen, wobei man sich unter ''Blockkette'' jedoch ohne Vorwissen nicht viel vorstellen kann. Vereinfacht gesagt, ist ein Block ein Teil der Ansammlung von Daten in der Blockchain. Wenn man auf die Vorstellung von analogen Hauptbüchern zurückgreift, kann man sich das in etwa wie eine Seite dieses Buches vorstellen. Der Begriffsteil ''Kette'' beschreibt die Vorgangsweise, wie diese Blöcke verbunden werden. In dem Fall der ''Chain'' sind die Blöcke einfach aneinander angehängt, wobei jeder Block, ausgenommen der jeweils erste und letzte, genau einen Vorgänger und genau einen Nachfolger hat. (vgl. \cite{Web:43})

\underline{Definitionen}

Aufgrund der bereits bei DLT genannten Probleme der Differenzierung dieser Begriffe, finden sich schwer Definitionen, welche genaue Abstriche zwischen der ''Distributed Ledger Technology'' und ''Blockchain'' machen. (vgl. \cite{Web:44}, \cite{Web:45}, \cite{Web:46})

Herausstechend ist die Definition erneut aus dem Gabler Wirtschaftslexikon:

\begin{quote}
	\textit{''Technisch stellt die Blockchain (''Blockkette'') eine dezentrale Datenbank dar, die im Netzwerk auf einer Vielzahl von Rechnern gespiegelt vorliegt. Sie zeichnet sich dadurch aus, dass ihre Einträge in Blöcken zusammengefasst und gespeichert werden. Durch einen von allen Rechnern verwendeten Konsensmechanismus wird die Authentizität der Datenbankeinträge sichergestellt. Oftmals wird der Überbegriff ''Distributed Ledger'' synonym verwendet, auch wenn nicht jeder Distributed Ledger unbedingt eine Blockkette verwendet.''}
	\cite{Web:47}
\end{quote}

Hierbei wird genau Abgegrenzt, dass eine Blockchain ein Distributed Ledger ist, jedoch nicht jeder Distributed Ledger eine Blockchain. Darüber hinaus ist das besondere Merkmal von Blockchains durch die Blöcke definiert, sowie die Anwendung eines bestimmten Konsensmechanismuses.

IBM hingegen verfolgt den Ansatz, sich speziell auf Vermögenswerte zu konzentrieren, wobei weitere Erläuterungen den Begriff durchaus gut definieren:

\begin{quote}
	\textit{''Blockchain is a shared, immutable ledger that facilitates the process of recording transactions and tracking assets in a business network. An asset can be tangible (a house, car, cash, land) or intangible (intellectual property, patents, copyrights, branding). Virtually anything of value can be tracked and traded on a blockchain network, reducing risk and cutting costs for all involved.''}
	\cite{Web:48}
\end{quote}

Speziell eingegangen wird dabei auf sogenannte ''tangible'' und ''intangible'' Assets, wobei erstere einen physischen Gegenwert, wie etwa ein Haus, besitzen und zweitere nicht, wie zum Beispiel ein Markenname.

Als Schlüsselelemente nennt IBM ''Distributed ledger technology'', ''Immutable records'' und ''Smart contracts''. Unter ''Immutable records'' versteht man, dass Einträge, welche einmal in der Blockchain existieren, nie wieder gelöscht werden können. ''Smart contracts'' sind vereinfacht eine Reihe an Anweisungen, welche in der Blockchain gespeichert werden, um automatisch bestimmte Transaktionen abzuwickeln. (vgl. \cite{Web:48}) 

\underline{\textbf{Wallet}}

Mit der Hilfe von Wallets können Besitzer von Kryptowährungen auf ihr Vermögen zugreifen, sowie ihre Coins und Tokens senden und empfangen. Um das zu ermöglichen, muss innerhalb der Blockchain bzw. des Netzwerkes eindeutig auf eine Wallet referenziert werden können. Das erfolgt durch die Wallet Adresse, welche vereinfacht einen Teil des public-keys darstellt, welcher neben dem private-key auch in der Wallet gespeichert werden muss. Das kann entweder in einer Hardware-Wallet oder einer Software-Wallet passieren. 

\underline{Software Wallet}

Bei einer Software-Wallet werden die Schlüssel direkt auf dem Computer gespeichert.\\
Vorteil davon ist, dass die Schlüssel sehr schnell verfügbar sind, wenn sie gebraucht werden. Oft werden diese Art von Wallets direkt als Browser-Erweiterung zur Verfügung gestellt.
Ein großer Nachteil ist, dass bei einer Sicherheitslücke am Computer, wie etwa wenn das Gerät durch einen Virus infiziert ist, die Schlüssel schnell gestohlen werden können. (vgl. \cite{Web:04}, \cite{Web:10})

\underline{Hardware Wallet}

Unter einer Hardware-Wallet versteht man meistens einen externen Datenträger, auf dem die Schlüssel gespeichert werden. Zwar werden auch Apps auf dem PC benötigt, um den Datenträger zu lesen, jedoch wird dabei nichts am Computer gespeichert.\\
Ein großer Vorteil ist hier, dass die Schlüssel nicht dauernd am PC mit Internetzugang gespeichert sind. Das heißt man kann den Datenträger sorglos unter dem Bettkissen verstauen.

Nachteil davon ist zum Beispiel das Risiko zum Verlust des Datenträgers. Darüber hinaus reduziert dies auch die Benutzerfreundlichkeit, da man immer dafür sorgen muss, den Datenträger bei sich zu haben, falls man die Schlüssel benötigt. (vgl. \cite{Web:04}, \cite{Web:10})

\underline{Wallet Adresse}

Die Wallet Adresse ist eine eindeutige Zeichenkette, die eine bestimmte Wallet identifiziert. Mithilfe der Wallet-Adresse können sehr einfach digitale Assets empfangen und gesendet werden. Dadurch benötigt der Sender nicht den ganzen Public-Key, sondern lediglich die Adresse der Wallet des Empfängers. In diesem Zusammenhang spricht man auch von einem ''vereinfachten Private-Key''. Diese Adresse kann man etwa mit einem IBAN vergleichen.
(vgl. \cite{Web:05})


\underline{\textbf{Bestandteile einer Transaktion}}

Unter einer Transaktion in der Blockchain versteht man, wie bei traditionellen Datenbanken, eine Anweisung, welche eine Datenbank, in diesem Fall die Blockchain, von einem konsistenten Zustand in einen anderen versetzt. Dabei befolgen die Transaktionen ebenfalls das ACID-Prinzip. Sie sind atomar und daher nur ganz oder gar nicht ausführbar (Atomicity), sie sorgen dafür, dass die Blockchain von einem konsistenten Zustand in den nächsten versetzt wird (Consistency), sie beeinflussen sich nicht gegenseitig (Isolation), und sie bleiben dauerhaft bestehen und können nicht gelöscht werden (Durability). (vgl. \cite{Web:77})

Je nach Blockchain werden Transaktionen technisch anders umgesetzt und es gibt auch zahlreiche Arten von Transaktionen. So etwa unterscheidet das Bitcoin-Protokoll zwischen ''Pay-to-PubkeyHash'', für simple Zahlungen, und ''Pay-to-Script-Hash'', für kompliziertere Transaktionen. Das Ethereum Protokoll verfolgt auch den Ansatz für zwei verschiedene Arten, wobei diese sich in reguläre Transaktionen, und ''Contract deployment transactions'', welche einzig und allein zur Veröffentlichung von Smart-Contracts verwendet werden, unterscheiden. (vgl. \cite{Web:78}, \cite{Web:79})

Beispielsweise besteht eine reguläre Transaktion im Ethereum-Protokoll aus folgenden sieben Datenfeldern (vgl. \cite{Web:78}): 

\textbf{- Recipient}: Der Empfänger der Transaktion\\
\textbf{- Signature}: Die digitale Signatur des Senders\\
\textbf{- Value}: Der Wert an Ethereum die gesendet werden\\
\textbf{- Data}: Optionales Feld für zusätzliche Daten\\
\textbf{- GasLimit}: Maximale Anzahl an Gas\footnote{Einheit zum vereinheitlichen von Benötigter Rechenleistung für eine Anweisung} Einheiten, welche die Transaktion verwenden darf\\
\textbf{- MaxPriorityFeePerGas}: Maximale Anzahl an Gas, die dem Miner als Trinkgeld gegeben werden\\
\textbf{- MaxFeePerGas}: Maximale Anzahl an Gas, die für die Transaktion bezahlt werden kann


\underline{\textbf{Bestandteile eines Blockes}}

Ein Block besteht aus einem ''Block Body'' und einem ''Block Header''. Je nach Blockchain Protokoll (Bitcoin, Ethereum, Cardano, ...) können diese unterschiedlich groß und auch verschiedene Daten enthalten, besonders wenn unterschiedliche Konsenstypen (PoW, PoS) verwendet werden. Die grundsätzliche Struktur ist jedoch bei allen Protokollen sehr ähnlich, siehe Abbildung \ref{fig:blockstructure}. Mit einem sogenannten ''Block-Explorer'' kann die Blockchain öffentlich im Internet eingesehen werden, somit auch die Daten der Blöcke und Transaktionen. Beispiele sind https://btc.com/ (Bitcoin) und https://etherscan.io/ (Ethereum).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{images/block_structure}
	\caption{Aufbau eines Blockes}
	\label{fig:blockstructure}
	\cite{Web:51}
\end{figure}

\underline{Block Nummer}

Alle bestätigten Blocke einer Blockchain werden aufsteigend nach einander Nummeriert. Der erste Block, der sogenannte \textbf{''Genisis Block''}, startet diese Nummerierung, auch ''Block-Height'' genannt, mit entweder 0 oder 1. Dieser Genesis Block ist auch der einzige, welcher keinen Vorgängerblock hat. (vgl. \cite{Web:54})

\underline{Block Header}

Der Block Header speichert die wichtigsten Informationen, um die Integrität innerhalb der Blockchain zu gewährleisten. 

Eines der sechs Datenfelder innerhalb des Block Headers ist die \textbf{Version}. Sie gibt an, welcher zu welchem Technologiestand der Blockchain dieser Block erstellt wurde.

Der wohl interessanteste Wert ist der ''\textbf{previous-block-hash}'', welcher immer auf den jeweiligen Vorgängerblock verweist. Zur Identifizierung wird dabei der Hash-Wert des Blockes verwendet, da dieser (zumindest praktisch) eindeutig ist.

An zweitwichtigster Stelle steht der ''\textbf{Merkle Tree Root}''. Ein Merkle Tree ist eine baumartige Datenstruktur, in welcher Datensegmente ihren Hash-Werten zugeordnet werden, und Hash-Werte auf gleicher Baumebene wieder zu einem Hash-Wert zusammengefasst werden. (vgl. \cite{Web:49})
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/merkle}
	\caption{Prinzip eines Merkle Baums}
	\label{fig:merkle}
	\cite{Web:49}
\end{figure}
Unter dem Merkle Tree Root versteht man die Wurzel dieses Baumes, also den ''Top-Hash''. Dieser fasst sozusagen alle darunter liegenden Daten in einen eindeutigen und kurzen Hash-Wert zusammen. Übertragen auf den Block bedeutet das, dass so ein Baum anhand der für den Block ausgewählten Transaktionen gebaut wird. Dabei werden die Daten in den Buchungen gehasht, was den eindeutigen Hash dieser ergibt. Die dabei entstehenden Hash-Werte werden dann so lange zusammengeführt und erneut gehasht, bis nur noch der Merkle Tree Root übrig bleibt. (vgl. \cite{Web:50})

Weiters wird der aktuelle \textbf{Zeitstempel}, zum Zeitpunkt der Erstellung des Blockes, festgehalten.

\underline{Block Body}

Der Körper eines Blockes enthält alle sonstigen Daten. Das sind zum Beispiel die Anzahl der enthaltenen Transaktionen und die Liste dieser. Aufgrund vieler Einschränkungen, stammend aus dem Konsensmechanismus, ist die Anzahl an möglichen Transaktionen innerhalb eines Blockes unterschiedlich begrenzt. (vgl. \cite{Web:53})


\underline{\textbf{Konsensmechanismus}}

Der ''Consensus Mechanism'' (deutsch: Konsensmechanismus) gibt vor wie sich Benutzer des distributed Ledgers auf einen bestimmten Zustand dessen einigen. Dadurch, dass jeder Nutzer unterschiedlich aktuelle Ledger besitzen kann, benötigt es hierbei sehr präzise angaben, an die sich jede Node\footnote{Computer welcher Blockvalidierung durchführt} halten muss. 

\underline{Entstehung eines Blockes}

Wenn eine neue Transaktion der Blockchain hinzugefügt werden soll, muss diese als aller erstes den im Netz vorhandenen Nodes bekannt gemacht werden. Dazu gelangt die Transaktion zuerst in eine Art Warteraum, auch ''Pool'' genannt, aus welchem die Nodes mehrere Buchungen auswählen, um sie in einen neuen Block einzuarbeiten. Nach der Validierung des Blockes durch die verarbeitende Node wird der bestätigte Block an alle anderen Nodes gesendet, der Blockchain angehängt und der Node eine gewisse Belohnung eingeräumt. Sobald das passiert ist, kann dieser Block nicht mehr verändert werden. (vgl. \cite{Web:55})

Nun braucht es jedoch Regeln dazu, welche feststellen, welche Node einen Block validieren darf, beziehungsweise wer einen Block anhängen darf, falls mehrere Blöcke gleichzeitig von unterschiedlichen Nodes verarbeitet werden, und vor allem wer welche Transaktionen durchführen darf. Diese Regeln werden durch den Konsensmechanismus festgelegt und verhindern somit Probleme wie ''Double Spending''\footnote{Wenn ein Benutzer der Blockchain versucht mehr Geld auszugeben, als ihm zusteht}. 

\underline{Definitionen}

Die vorhandenen Definition von Konsensmechanismen sind im allgemeinen sehr deutlich und einheitlich formuliert.

\begin{quote}
	\textit{''A consensus mechanism is a fault-tolerant mechanism used in a blockchain to reach an agreement on a single state of the network among distributed nodes. These are protocols that make sure all nodes are synchronized with each other and agree on transactions, which are legitimate and are added to the blockchain.''}
	\cite{Web:41}
\end{quote}

Immer wieder auftretend ist hierbei der Begriff ''fault-tolerant'' sowie das Ziel, sich auf einen eindeutigen Zustand eines Netzwerkes zu einigen.

Unter ''fault-tolerance'' beziehungsweise ''Fehlertoleranz'' eines solchen Mechanismus versteht man die Fähigkeit, trotz etwaiger Unstimmigkeiten oder Fehler im Netzwerk immer wieder einen eindeutigen Zustand ermitteln zu können. (vgl. \cite{Web:42})

\underline{Proof of Work}

Proof-of-Work, kurz auch PoW genannt, war eine der ersten Konsensmechanismen, welche auf Blockchains eingesetzt wurden. Die bekannteste davon ist wahrscheinlich Bitcoin, mit Ethereum dicht gefolgt.

Die Nodes, welche bei diesem Typen von Konsensmechanismus die Blöcke validieren, nennt man \textbf{Miner}. Das sind Computer, welche auf der Welt verteilt und mit dem Blockchain-Netzwerk verknüpft sind, um diesem ihre Rechenleistung zur Verfügung zu stellen. Sie kämpfen darum, erster bei der Validierung eines Blockes zu sein, um eine Belohnung zu erhalten. (vgl. \cite{Web:57})

Die Funktionsweise von PoW beruht auf zwei wesentlichen Erweiterungen des Block-Header. Zum einen wurde ein Feld \textbf{''Difficulty''} hinzugefügt, welches den Schwierigkeitsgrad für eine mit Rechenleistung zu lösende Aufgabe darstellt. Des weiteren wird das Datenfeld \textbf{''Nonce''} benötigt, in welchem die Lösung zu dieser Aufgabe vom Miner einzutragen ist. (vgl. \cite{Web:57})

Die Aufgabe die hierbei zu lösen ist, kann von Netzwerk zu Netzwerk unterschiedlich sein. Am einfachsten zu erklären ist es am Beispiel vom Bitcoin-Protokoll:

\begin{quote}
	\textit{''To implement a distributed timestamp server on a peer-to-peer basis, we will need to use a proofof-work system similar to Adam Back's Hashcash, rather than newspaper or Usenet posts.
		The proof-of-work involves scanning for a value that when hashed, such as with SHA-256, the
		hash begins with a number of zero bits. The average work required is exponential in the number
		of zero bits required and can be verified by executing a single hash.
		For our timestamp network, we implement the proof-of-work by incrementing a nonce in the
		block until a value is found that gives the block's hash the required zero bits. [...]''}
	\cite{Web:15}
\end{quote}

Dieser Auszug aus dem Bitcoin-Whitepaper beschreibt die Vorgangsweise. Das Ziel ist, den gesamten Block so lange erneut zu hashen, bis sich ein Hash-Wert ergibt, welcher mit einer durch die Difficulty bestimmten Anzahl an Nullen startet. Damit sich dabei verschiedene Hashwerte ergeben, wird das Nonce-Datenfeld, welches sich im Block-Header befindet, immer um eins erhöht.

Eine weitere wichtige Eigenschaft dieser Vorgangsweise aus dem Bitcoin-Whitepaper ist wie folgt:

\begin{quote}
	\textit{''[...] Once the CPU
		effort has been expended to make it satisfy the proof-of-work, the block cannot be changed
		without redoing the work. As later blocks are chained after it, the work to change the block
		would include redoing all the blocks after it.''}
	\cite{Web:15}
\end{quote}

Übersetzt bedeutet das, dass ein Block, welcher einmal validiert wurde, nie wieder veränderbar ist, es sei denn man tut sich die Arbeit an, die Lösung zu dem neuen Problem zu finden. Wenn mehrere Blöcke aneinander hängen, bedeutet das auch, dass die Lösung auch für diese Blöcke nun anders ist, und die Validierung ebenfalls erneut stattfinden müsste.

Die Erklärung dafür ist, dass der Hash-Wert, welcher beim finden der Lösung herauskommt, je nach Inhalt des Blockes ein anderer ist. So hat zum Beispiel ein Block mit 76 Transaktionen als Lösung 42. Kopiert man jedoch diesen Block und möchte nun eine Transaktion entfernen oder einfügen, ändert sich die Lösung auf 13. Das gleiche Prinzip gilt auch für nachfolgende Blöcke. Wenn sich der Inhalt eines Vorgängers ändert, so ändert sich auch dessen Hash-Wert und somit seine Adresse. Der Nachfolger hat nun einen ''previous-block-hash'' welcher auf einen nicht existierenden Block referenziert, somit ist dieser Block, sowie alle seine Nachfolger, ungültig. (vgl. \cite{Web:56})


\underline{PoW Beispiel}

Eine Webseite gebaut von Anders Brownworth (vgl. \cite{Web:58}) dient hier besonders gut zur bildlichen Darstellung dieses Prinzips und hat sich auf Sozial-Media Kanälen im Krypto-Bereich dadurch besonders berühmt gemacht.

Zu aller erst wird darauf eingegangen, wie genau \textbf{Blöcke} gehasht werden, und wie durch Änderungen in ihren Daten ein komplett anderer Hash-Wert entsteht:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/block_01}
	\caption{Block auf andersbrownworth.com}
	\label{fig:block01}
\end{figure}

Zur simplifizierten Darstellung wurden nur Zeitdaten und exemplarische Transaktionen im Datenfeld eingetragen.
Man sieht auf Abbildung \ref{fig:block01} nun einen gültigen Block mit dem Hash-Wert von ''000094e76516cb137611f717f39472e70...''. Hier erkennt man gut, dass diese Demo auf dem gleichen Weg wie auch Bitcoin Blöcke validiert, nämlich anhand der vier vorangestellten Nullen. Diese ''4'' kann sich als die Difficulty unseres Beispiels vorstellen.

Wird nun dieser schon validierte Block im Nachhinein geändert, erscheint er mit der Farbe Rot hinterlegt:
 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/block_02}
	\caption{Die Transaktion T4 wurde hinzugefügt}
	\label{fig:block02}
\end{figure}

Das liegt daran, dass nach dem hinzufügen der Transaktion ''T4'', der Block einen völlig anderen Hashwert besitzt, als zuvor. Man erkennt, dass der Hash ''a67ed746ab6af585fc7be4a1...'' nun nicht mehr mit vier Nullen beginnt, somit ist er ungültig. 

Betätigt man nun die Schaltfläche ''Mine'', so wird der modifizierte Block erneut validiert und er erscheint Grün:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/block_03}
	\caption{Erneut validierter Block}
	\label{fig:block03}
\end{figure}

Nun hat sich das Feld ''Nonce'' geändert, da durch die Validierung, also das Minen, eine neue Lösung für den Block gefunden werden musste, damit der Hash wieder mit vier Nullen beginnt. Dazu hat der Miner, in diesem Fall wir mit unserem Javascript fähigem Browser, dieses Feld von 0 weg immer um eins erhöht, bis dieser Hash gefunden wurde.

Würde man den exakt selben Block erneut Minen, würde sich in diesem Fall auch der exakt gleiche Hashwert ergeben. Das gilt natürlich nicht, wenn sich der Zeitstempel in der Zwischenzeit ändert.


Als nächstes wird die \textbf{Blockchain} erklärt. 

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/blockchain_01}
	\caption{Ausgangssituation Blockchain}
	\label{fig:blockchain01}
\end{figure}

In Abbildung \ref{fig:blockchain01} werden 5 verschieden Blöcke dargestellt, welche der Reihe nach Nummeriert und mit Transaktionen befüllt sind. Ebenfalls ist zu erkennen, dass alle Blöcke bereits validiert sind, da alle Hashwerte mit ''0000'' beginnen. Der Unterschied zu dem vorherigen Beispiel ist, dass nun das Feld ''Prev'' hinzugekommen ist. Dieses Feld stellt den ''previous-block-hash'' dar. Wenn man sich die Werte darin ansieht, erkennt man, dass jeder Block, außer der Genesis Block, dort seinen Vorgänger eingetragen hat. Nun erinnert die Struktur schon viel eher an eine Kette, da jeder Block mit dem vorherigen verkettet ist.

Wenn man nun in dieser Struktur am letzten Block etwas ändert, passiert genau das selbe wie bei dem vorherigen Beispiel. Der Block wird Rot, und um ihn erneut zu validieren müsste man ihn neu minen. Etwas Besonderes passiert nun aber, falls man einen von seinen Vorgängern verändert:

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/blockchain_02}
	\caption{Dem Block Nr. 4 wurde eine neue Transaktion TX hinzugefügt}
	\label{fig:blockchain02}
\end{figure}

Durch das hinzufügen einer neuen Transaktion im Block mit der Nummer 4, hat sich bekannterweise auch sein Hash geändert. Das Phänomen hierbei ist jedoch, dass nun der neue Hash auch in den nachfolgenden Block als previous-block-hash übernommen wurde. Dadurch haben sich auch die Daten im Block Nr. 5 geändert, wodurch sich wiederum sein Hash verändert hat. Als Folge der Änderung am 4. Block sind nun er und alle seine Nachfolger ungültig. Um diese Diskrepanzen nun zu beheben, müsste man alle Nachfolgeblöcke des veränderten Blocks erneut minen.

Da nun das Blockchain-Netzwerk aus mehreren Nodes besteht, wobei jede ihre eigene Version der Blockchain (bzw. des Ledgers) haben kann, muss klar gemacht werden, welcher Version ein Benutzer glauben soll. Dabei nimmt man ganz einfach an, dass die Version, in der die meiste Arbeit, und somit die meisten Blöcke, steckt, die richtige ist. 

\underline{Proof-of-Stake}

Proof-of-Stake ist die zweit meist verwendete Konsensart in Blockchain-Netzwerken. Sie versucht einige Probleme von Proof-of-Work auszuhebeln, wie etwa den enormen Stromverbrauch oder die Dominanz besserer Hardware, schafft dabei jedoch auch neue.

Anstatt der von PoW bekannten Miner, verwendet PoS sogenannte \textbf{Validator}, welche für das Bestätigen von Blöcken zuständig sind. Der Validierungsprozess wird hier \textbf{Minting oder Forging} genannt.

Um ein Validator zu werden, reicht es nun nicht mehr nur Rechenleistung zur Verfügung zu stellen, sondern es muss ein gewisser Startbetrag an Kryptowährung hinterlegt werden (\textbf{Stake}), welcher als Kaution verwendet wird. Dieser Betrag von Blockchain zu Blockchain einen anderen Mindestwert. Außerdem ist nun nicht mehr jeder dazu berechtigt, gleichzeitig einen Block zu validieren. Es wird in einem Zufallsverfahren ein Validator ausgewählt, wobei die Wahrscheinlichkeit, dass man gewählt wird, von der Kautionshöhe abhängt. (vgl. \cite{Web:69})

Falls nun ein Validator Fehler bei der Bestätigung des Blockes macht oder versucht die Blockchain zu manipulieren, bemerken dies die anderen Nodes bei der Kontrolle. Um dem entgegen zu wirken, kann das Netzwerk dem bösartigen Validator seine Kautionsleistung entziehen oder nur teilweise zurückgeben (\textbf{Slashing}) und ihn vom Netzwerk sperren. (vgl. \cite{Web:70})

Möchte ein Validator nun selbst aus dem Netzwerk aussteigen, bekommt dieser seine Kaution inklusive seiner verdienten Belohnungen zurück. Dieser Prozess ist eine gewisse Zeit verzögert, um eventuell im Nachhinein anfallende Strafen abziehen zu können.

Ein Beispiel für ein zukünftiges Proof-of-Stake Netzwerk ist Ethereum. Ethereum durchläuft zurzeit einen wechsel des Konsensmechanismuses, weg von der alte Proof-of-Work Methode. Um Validator für das Ethereum Netzwerk zu werden, muss ein Benutzer 32 Ethereum als Stake zur Verfügung stellen, was umgerechnet zum Kurs vom 07.02.2022 circa 85.500 Euro entspricht. (vgl. \cite{Web:71})

\underline{Alternative Konsensmechanismus-Typen}

Obwohl in der Praxis derzeit nur sehr wenig verschiedene Konsensmechanismen abseits von Proof-of-Work und Proof-of-Stake verwendet werden, gibt es zahlreiche Alternativen dazu. Je nach Anwendungsbereich sind diese unterschiedlich gut geeignet. (vgl. \cite{Web:72})

Einige alternativen funktionieren folgendermaßen:

Bei \textbf{Delegated Proof of Stake} werden ähnlich wie bei PoS Validator ausgewählt. Diesmal werden diese jedoch von den Benutzern der Blockchain in einem Wahlverfahren ausgewählt. Die gewählten Nodes validieren im Round-Robin-Verfahren Blöcke.

Bei \textbf{Proof of Authority} geben Nodes Ihre echte Identität preis und haften auch damit, falls diese Fehler begehen.

\textbf{Proof of Capacity} funktioniert ähnlich wie PoW, nur wird hier anstatt Rechenleistung Speicherplatz zur Validierung verwendet.

Bei \textbf{Proof of Burn} müssen Nodes Kryptowährung an eine nicht existente Adresse schicken, um diese zu ''verbrennen''. Je nach Betrag dürfen diese neue Blöcke validieren.


\underline{\textbf{Anomalien in der Blockchain}}

Im folgenden werden einige Angriffsmöglichkeiten auf Blockchain-Protokolle, als auch andere Wege wie sich eine Blockchain in ihrer Lebenszeit verändern kann, näher erklärt.

\underline{51\%-Attacken}

Durch die Annahme, dass die längste Blockchain immer die richtige ist, wird ein  sehr wichtiges Problem gelöst. Man nehme an, ein Miner ist bösartig und möchte eine Transaktion in einen Block einschleusen, bei welchem er durch Glück der schnellste war, ihn zu validieren. In dieser Buchung überweist er einem Opfer Geld, jedoch sendet er den validierten Block nur an den Benutzer, welchem er Geld überwiesen hat, und nicht an alle Benutzer des Netzwerkes, damit für diese sein alter Kontostand bestehen bleibt. Er hat nun aus der Sicht des Opfers diese Überweisung getätigt, jedoch aus der Sicht des restlichen Netzwerkes kein Geld ausgegeben. Diese Betrugsmethode wird auch als Double-Spending bezeichnet.

Das kann theoretisch in dieser Art nicht ausgeschlossen werden, jedoch spielt hier die Größe des Netzwerkes eine große Rolle. Um diese ''Lüge'' aufrecht zu halten, müsste der bösartige Miner nun alle nachfolgenden Blöcke ebenfalls selbst minen, und an unser Opfer senden, weil dieses immer der längsten Blockchain glaubt. Hier kommt es nun auf die Rechenleistung des Miners an. Hat dieser weniger als 50\% der Rechenleistung des gesamten Netzwerkes, ist die Wahrscheinlichkeit in etwa bei Null, dass er Blöcke immer schneller validiert, als der Rest des Netzwerkes. Somit wird die Version des restlichen Netzwerkes innerhalb einer gewissen Zeit länger als die des Hackers. Beträgt die Rechenleistung des Miners jedoch mehr als 50\%, bezeichnet man dies als eine 51\%-Attacke. Der Hacker ist nun schneller als das Netzwerk und somit wird seine Blockchain als die richtige erkannt. So sind bei kleineren Netzwerken mit wenig Gesamtrechenleistung solche Attacken einfacher möglich. (vgl. \cite{Web:56}, \cite{Web:64})

\begin{quote}
	\textit{''Die Wahrscheinlichkeit, das Netzwerk mit einer 51\%-Attacke zu kontrollieren, steigt mit der relativen Hashrate des Angreifers. Das heißt: Derselbe Angriff ist auch mit weniger als 51 Prozent möglich, allerdings ist die Wahrscheinlichkeit nicht auf Seite der Angreifer. Langfristig hat die Mehrheitsattacke nur Sinn, wenn der Angreifer auch tatsächlich die Mehrheit der Hashrate kontrolliert. ''}
	\cite{Web:65}
\end{quote}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/52_proba}
	\caption{Wahrscheinlichkeit einer 51\%-Attacke}
	\label{fig:52proba}
	\cite{Web:65}
\end{figure}

Abbildung \ref{fig:52proba} zeigt das Verhältnis der Wahrscheinlichkeiten, dass ein Angreifer eine 51\%-Attacke erfolgreich durchführen kann. Die Anzahl der Blöcke bezieht sich dabei auf die Anzahl der vom Angreifer hintereinander bestätigten Blöcke, die sein Teilnetzwerk bestätigen kann.


\underline{Forks}

Ein viel natürlicherer Prozess ist ein sogenannter Fork. Dabei handelt es sich nicht um einen Angriffstyp, sondern eine Art wie sich die Blockchain in zwei verschiedene Teile aufteilen kann. Sie ''Gabelt'' sich. (vgl. \cite{Web:63})

Bei einem \textbf{Soft Fork} wird eine Änderung in das Blockchain-Protokoll eingeführt, welche rückwärts kompatibel ist. Nodes müssen dabei nicht unbedingt eine Entscheidung treffen, sie können weiterhin wie gehabt neue Blöcke anhängen, jedoch wird ein Großteil der anderen Nodes auf die Änderungen eingehen und eine verbesserte Blockchain fortführen. Die ursprüngliche Blockchain veraltet. (vgl. \cite{Web:61}, \cite{Web:62})

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/soft_fork}
	\caption{Soft-Fork Beispiel}
	\label{fig:softfork}
	\cite{Web:59}
\end{figure}


Bei einem \textbf{Hard Fork} entschließt sich ein Teil der Nodes, eine Änderung, welche nicht rückwärts kompatibel ist, an der Blockchain vorzunehmen. Diese Meinung muss nicht von allen Nodes geteilt werden, jedoch müssen sie eine Entscheidung treffen, und Modifizierungen an ihrer eigenen Software vornehmen, falls sie weiterhin als Node fungieren möchten. Die Nodes, welche der Meinung sind, dass die Änderung keine gute Entscheidung ist, ändern ihre Software so, damit sie neue Blöcke an der originalen unveränderten Blockchain anhängen können. Der anderer Teil jedoch ändert die Software ab, damit an der veränderten Chain neue Blöcke angehängt werden können. (vgl. \cite{Web:60}, \cite{Web:62})

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/hard_fork}
	\caption{Hard-Fork Beispiel}
	\label{fig:hardfork}
	\cite{Web:59}
\end{figure}

Ein praktisches Beispiel für ein Hard-Fork ist der Ethereum und Ethereum-Classic Split aus dem Jahr 2016. Zustande gekommen ist dieser durch eine Cyberattacke auf die damals sehr beliebte autonome Investmentfirma auf der Ethereum Blockchain, genannt \textit{The DAO}. Ihr Geschäft bestand daraus, Stimmungsberechtigungen auf Ethereum zu verkaufen, wofür sie innerhalb von etwa einem Monat ca. 150 Mio. Euro in Form von Ethereum durch Crowdfunding ergattert hatten. Durch einen Fehler in einem Smart-Contract dieser Organisation wurde jedoch eine Sicherheitslücke ausgenutzt, wodurch ein Hacker rund 60 Mio. Euro entwenden konnte. Obwohl ein Hard-Fork daraufhin sehr umstritten war, entschloss sich die Ethereum-Community letztendlich doch dazu, diesen Hack rückgängig zu machen. Dazu wurde die Blockchain auf einen Block kurz vor der Attacke zurückgesetzt. Dadurch, dass jedoch nicht alle Mitglieder damit zufrieden waren, hat sich aus der veralteten Blockchain, auf welcher der Diebstahl immer noch verbucht ist, die Ethereum-Classic Blockchain fortgesetzt. (vgl. \cite{Web:66}, \cite{Web:67})

Bei einem Fork werden bestehende Einheiten an Kryptowährungen verdoppelt. Das heißt, dass ein Benutzer, der vor dem Ethereum-Split etwa 2 Ethereum besessen hat, nun 2 Ethereum Coins auf der originalen Blockchain besitzt, aber auch 2 Ethereum-Classic Coins. Es scheint als würde hierbei einfach Geld verdoppelt werden, was jedoch nicht stimmt. Ein Fork einer Blockchain hat enorme Auswirkungen auf den Markt und somit ergeben sich für die zwei Währungen komplett neue Preise, je nach dem welche Blockchain sich dann durchsetzt, beziehungsweise wie viel Wert ihr durch den Markt zugesagt wird. (vgl. \cite{Web:68})

