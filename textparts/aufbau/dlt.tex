\subsection{Distributed Ledger Technology}

Unter der ''Distributed Ledger Technology'' versteht man die Technologie bzw. das Prinzip hinter den heute bekannten dezentralen Währungen wie Bitcoin oder Ethereum. 

\underline{\textbf{Ledger in der Finanzwelt}}

Unter einem Ledger (Englisch für Hauptbuch oder Kontoführungsbuch) versteht man in der Finanzwelt ein Dokument in dem Kontodaten sowie zugehörige Buchungen einheitlich mit Zeitstempel dokumentiert werden. Mithilfe des Ledgers können Konten identifiziert und auch Transaktionen dieser verfolgt werden. Heutzutage sind Ledger über große Datenbanken implementiert. (vgl. \cite{Web:32})

Dabei können Ledger auf zwei verschiedene Arten verwaltet werden:

\underline{Zentrale Ledger}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{images/centralized_ledger}
	\caption{Aufbau zentralisierte Ledger}
	\label{fig:centralizedledger}
	\cite{Web:31}
\end{figure}

Bei einem zentralisierten Ledger wird das Hauptbuch zentral von einer Institution verwaltet. Dieses Institut kann eine Bank oder auch ein Spielekasino, welches Jetons verteilt, sein. Dabei gibt es eine Datenbank in der alle Transaktionen aufgezeichnet werden.

Der Vorteil von zentralen Ledgern ist der gesteuerte Zugriff. Durch die Reduktion der Schnittstellen kann eine exakte Zugriffskontrolle durchgeführt werden und bestimmt werden, wer den Ledger einsehen und beschreiben darf und wer nicht. 

Der Nachteil ergibt sich, falls diese zentrale Schnittstelle bösartig ist, oder nicht so handelt wie angenommen. Dadurch, dass nur das zentrale Institut genau über alle Vorgänge Bescheid weiß, müssen alle Kunden diesem Institut vollständig vertrauen. Außerdem werden Entscheidungen über Änderungen im Ledger ausschließlich von der zentralen Einheit getroffen. (vgl. \cite{Web:31})


\underline{Dezentrale Ledger}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{images/decentralized_ledger}
	\caption{Aufbau dezentralisierte Ledger}
	\label{fig:decentralizedledger}
	\cite{Web:31}
\end{figure}

Bei dezentralen Ledgern gibt es mehrere Ledger, die jedoch den gleichen Inhalt haben. Diese Ledger werden auf mehrere Teilnehmer im Netzwerk verteilt, somit kann jeder eine eigene Kopie des Ledgers besitzen. Werden Änderungen am Ledger vorgenommen, so müssen alle Ledger im Netzwerk diese Änderung ebenfalls übernehmen. 

Ein Vorteil hierbei ist, dass die Benutzer selbst Kontrolle über ihre Transaktionen haben. Darüber hinaus gibt es keinen ''single point of failure'', das heißt, dass wenn ein Ledger ausfallen sollte, nicht das ganze Netzwerk ausfällt, so wie es bei Systemen mit zentralem Ledger sein würde. Weiters kann jeder Benutzer den Ledger selbst einsehen und muss keiner dritten Partei sein volles Vertrauen schenken. (vgl. \cite{Web:31})

Nachteil ist, dass alle Ledger immer auf dem gleichen Stand sein müssen, was auch mehr Bandbreite und Speicherplatz benötigt. Das Verteilen der Ledger schränkt jedoch sehr die Möglichkeiten der Privatsphäre ein und ruft nach neuen Lösungen in diesem Bereich.

\underline{\textbf{Definitionen}}

Die Technologie eines verteilten Hauptbuchs wird im Allgemeinen sehr breit definiert. Einige Quellen erwähnen hierbei lediglich die Verwendung mehrerer Ledger, wobei sich andere direkt auf die Verwendung von Währung beziehen, wobei das Prinzip auf wesentlich mehr Gebiete Anwendung findet. Viele davon überspringen auch gerne die Unterschiede zwischen Blockchain und DLT, wobei Blockchain lediglich eine Implementierung von DLT darstellt.

So etwa eine etwas schwammige Formulierung durch Bosch. Bezogen wird sich dabei konkret auf Kontenbücher.

\begin{quote}
	\textit{''DLT beschreiben dezentrale und digital geführte Kontenbücher. Durch die Möglichkeit, Informationen mit einem hohen Maß an Transparenz und Sicherheit zu verteilen, haben DLT das Internet tatsächlich weiterentwickelt.''}
	\cite{Web:33}
\end{quote}

Einfach zu verstehen, und auch akkurat wird DLT von SAP beschrieben. Dabei wird auch der Bezug zu Blockchain richtig definiert. Anzumerken ist hierbei, dass ein Teilnehmer derjenige ist, der eine Kopie des Ledgers besitzt, und nicht derjenige der Transaktionen tätigt. %todo??

\begin{quote}
	\textit{''Ein Distributed Ledger ist eine Datenbank mit Transaktionen, die über mehrere Computer und Standorte geteilt und synchronisiert wird. Eine zentrale Kontrollinstanz gibt es nicht. Jeder Teilnehmer besitzt eine identische Kopie des Datensatzes, der automatisch aktualisiert wird, sobald Ergänzungen vorgenommen werden. Die Blockchain ist eine Art von Distributed Ledger.''}
	\cite{Web:34}
\end{quote}

Investopedia, eine Internetplattform mit Millionen an Lesern, welche versucht Finanzinformationen, Begriffe und Entscheidungen einfacher zu formulieren, macht jedoch einen falschen Bezug zu Blockchain:

\begin{quote}
	\textit{''Distributed Ledger Technology (DLT) refers to the technological infrastructure and protocols that allows simultaneous access, validation, and record updating in an immutable manner across a network that's spread across multiple entities or locations.
	\textbf{DLT, more commonly known as the blockchain technology}, was introduced by Bitcoin and is now a buzzword in the technology world,[...]''}
	\cite{Web:35}
\end{quote}

Eine sehr ausführliche und korrekte Beschreibung liefert das Gabler Wirtschaftslexikon. Hier wird das System der Ledger besonders im Zusammenhang der Datenverarbeitung erklärt. Ebenfalls angemerkt wurde die Verwendung eines Aktualisierungsprozesses, welcher für Einhaltung der Konsistenz innerhalb des Netzwerkes zuständig ist.

\begin{quote}
	\textit{''Bei der Distributed Ledger Technologie (DLT) handelt es sich um eine spezielle Form der elektronischen Datenverarbeitung und -speicherung. Als Distributed Ledger oder „Verteiltes Kontenbuch“ wird eine dezentrale Datenbank bezeichnet, die Teilnehmern eines Netzwerks eine gemeinsame Schreib- und Leseberechtigung erlaubt. Im Gegensatz zu einer zentral verwalteten Datenbank bedarf es in diesem Netzwerk keiner zentralen Instanz, die neue Einträge in der Datenbank vornimmt. Neue Datensätze können jederzeit von den Teilnehmern selbst hinzugefügt werden. Ein anschließender Aktualisierungsprozess sorgt dafür, dass alle Teilnehmer jeweils über den neuesten Stand der Datenbank verfügen. Eine besondere Ausprägung der DLT ist die Blockchain.''}
	\cite{Web:36}
\end{quote}

Die Worldbank bezieht sich bei der Definition sehr stark auf Blockchain, wobei die einzelnen Teile besser dargestellt werden können, da Blockchain als konkrete Implementierung zur besseren Veranschaulichung beiträgt. Dabei werden auch wichtige Fachbegriffe für diese Teile erwähnt. ''Nodes'' sind hierbei die Entitäten, welche eine Kopie des Ledgers  zur Verfügung stellen und verwalten. Ebenfalls erleutert wird der Begriff ''internet of value'', wobei ''value'' besonders für das Eigentum eines ''Vermögenswertes'' steht, was sich aber auch auf andere nicht monetäre Daten übertragen lässt.

\begin{quote}
	\textit{''Blockchain is one type of a distributed ledger. Distributed ledgers use independent computers (referred to as nodes) to record, share and synchronize transactions in their respective electronic ledgers (instead of keeping data centralized as in a traditional ledger). Blockchain organizes data into blocks, which are chained together in an append only mode.\\\\
	Blockchain/ DLT are the building block of “internet of value,” and enable recording of interactions and transfer “value” peer-to-peer, without a need for a centrally coordinating entity. “Value” refers to any record of ownership of asset -- for example, money, securities, land titles -- and also ownership of specific information like identity, health information and other personal data.''}
	\cite{Web:37}
\end{quote}


\underline{\textbf{Digitale Ledger}}

In diesem Abschnitt werden einzelne Bestandteile kurz erklärt, welche notwendig sind, um einen Distributed Ledger digital umzusetzen. 

Bekannterweise ergeben sich bei der Digitalisierung von Hauptbüchern, vor allem wenn diese öffentlich zugänglich und veränderbar sind, große Probleme. Eines davon ist zum Beispiel, dass jeder Teilnehmer eines DLT Netzwerkes den öffentlichen Ledger zu seinem Vorteil manipulieren könnte. Die Nutzer des Netzwerkes wüssten nun nicht mehr, welchem Ledger sie vertrauen sollen. Um diese Herausforderung zu überwinden, werden Digitale Signaturen bzw. Hashfunktionen angewendet, um einen sogenannten ''Consensus Mechanism'', übersetzt Konsensmechanismus, bereitzustellen.  

\underline{Digitale Signaturen}

Digitale Signaturen werden, genauso wie im analogen Büroverkehr, als Identifizierungsmerkmal einer Person verwendet. Dabei werden Dokumente oder Nachrichten mit einer Unterschrift versehen, um deren Echtheit sicherzustellen. Wichtig anzumerken ist, dass eine digitale Signatur jedoch nicht zum Verschlüsseln einer Nachricht dient. Zwar gibt es mittlerweile auch Möglichkeiten, seine händische Unterschrift zu digitalisieren, wie etwa die Eingabe über eine Touchdisplay, das kommt jedoch mit einigen Nachteilen, wie etwa, dass dieser einfache Text leicht gefälscht werden kann.

Zur Hilfe kommt hier die Zahlentheorie bzw. Kryptographie. Die Benutzung von Public bzw. Private Keys zur Verschlüsselung von Signaturen macht es beinahe unmöglich diese Art von Unterschriften zu fälschen. Der Public Key stellt hierbei die öffentliche Identität der Person dar, wie etwa in der analogen Welt ihren Namen. Den Private Key kann man sich wie die Hand einer Person vorstellen, er wird benötigt, um die eindeutige Unterschrift abzugeben. Diesen Private Key muss der Besitzer geheim halten und darf ihn keinen Falls verlieren. (vgl. \cite{Web:39})

Um nun eine Nachricht digital zu signieren, muss der Absender diese zuerst Hashen. Die Zeichenkette, welche dabei entsteht, muss dann mit dem Private-Key verschlüsselt werden. Das Ergebnis ist die digitale Signatur für diese bestimmte Nachricht. Beim Absenden dieser wird nun die digitale Signatur angehängt, sowie der Public-Key des Absenders und die verwendeten kryptografischen Algorithmen mit übermittelt. (vgl. Abb. \ref{fig:digsignature}, \cite{Web:38})

Ein beliebiger Empfänger kann nun die Signatur der Nachricht entnehmen, und mithilfe des mitgesendeten Public-Keys und der Angabe der verwendeten Algorithmen die Unterschrift, dank asymmetrischen kryptografischen Verfahren, entschlüsseln. Gleichzeitig wendet der Empfänger den überlieferten Hash-Algorithmus auf die Nachricht an. Bei der Entschlüsselung der Signatur erhält der Empfänger eine Hash-Zeichenkette, welche bei einer korrekten Signatur nun mit der Hash-Zeichenkette der Nachricht übereinstimmen muss. (vgl. Abb. \ref{fig:digsignature}, \cite{Web:38}, \cite{Web:40})

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/dig_signature}
	\caption{Ablauf einer digitalen Signatur}
	\label{fig:digsignature}
	\cite{Web:38}
\end{figure}
%todo: technisches Beispiel

